function find(elements, cb){
    if (Array.isArray(elements)){
        let index = 0;
        for (let element of elements) {
            if (cb(element)) {
                return index;
            } else {
                index++;
            }
        }
    } else {
        return ;
    }
}

module.exports = find;