function reduce(elements, cb, startingValue){

    let accumulator = startingValue;
    if (Array.isArray(elements)) {
        for (let current of elements) {
            accumulator = cb(accumulator, current);
        }

        return accumulator;
    } else {
        return ;
    }
}

module.exports = reduce;