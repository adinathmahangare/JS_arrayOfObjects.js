function each(elements, cb) {

    let resultArray = [];
    
    if (Array.isArray(elements)) {
        
        for (let element of elements) {
            result = cb(element);
            resultArray.push(result);
        }

        return resultArray;
    } else{
        return ;
    }
}

module.exports = each;