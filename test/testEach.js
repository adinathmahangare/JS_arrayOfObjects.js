const arrayData = require('../array');
const each = require('../each');

///created a callback function which prints all the elements in the given array
// function cb(i) {
//     console.log(i);
// }

///testing each function for every elememt inside array
function testEach(){
    each(arrayData, (element) => console.log(element));
}

testEach();