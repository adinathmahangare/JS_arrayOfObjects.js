function map(elements, cb){
    let resultArray = [];
    if (Array.isArray(elements)) {
        let result;
        for (let element in elements) {
            result = cb(element);
            resultArray.push(result);
        }
    }

    return resultArray;
}

module.exports = map;