function flatten(elements){

    let resultArray = [];
    
    if(Array.isArray(elements)){
        makeFlat(elements, resultArray);

        return resultArray;
    }else{
        return ;
    };
}

function makeFlat(elements, resultArray) {
    for (let index = 0; index < elements.length; ++index) {
        if(Array.isArray(elements[index])){
            makeFlat(elements[index], resultArray);
        }else{
            resultArray.push(elements[index]);
        }
        
    }

    return resultArray;
}

module.exports = flatten;